@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MIS LIBROS</div>

                <div class="card-body">
                    <a href="{{  route('tareas.create')  }}" class="btn btn-primary mb-3">Nuevo Libro</a>
                </div>
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Título</th>
                      <th scope="col">Fecha de Publicación</th>
                      <th scope="col">Lector</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Estado</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tasks as $task)
                    <tr>
                      <th scope="row">{{ $task->id }}</th>
                      <td>{{ $task->title }}</td>
                      <td>{{ $task->deadline }}</td>
                      <td>{{ $task->user->name }}</td>
                      <td>{{ $task->description }}</td>
                      <td>@if($task->is_complete == false)
                      <span class="badge badge-warning">Por Comprar</span>
                      @else
                      <span class="badge badge-warning">Comprado</span>
                      @endif
                      </td>
                      <td>@if($task->is_complete == false)
                      <a href="{{ route('tareas.status', $task->id) }}" class="btn btn-outline-success btn-sm"><ion-icon name="checkmark-circle-outline"></a>
                      @endif
                      <a href="{{ route('tareas.edit', $task->id) }}" class="btn btn-outline-info btn-sm">Editar</a>
                      <form method="POST" action="{{ route('tareas.destroy', $task->id) }}">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button type="submit" class="btn btn-danger btn-sm">Borrar</button>
                      </form>
                    </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection